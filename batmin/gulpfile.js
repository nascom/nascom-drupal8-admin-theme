var production    = false;

var fs            = require('fs'),
    del           = require('del'),
    gulp          = require('gulp'),
    autoprefixer  = require('gulp-autoprefixer'),
    concat        = require('gulp-concat'),
    eslint        = require('gulp-eslint'),
    imageoptim    = require('gulp-image-optimization'),
    insert        = require('gulp-insert'),
    prettify      = require('gulp-jsbeautifier'),
    newer         = require('gulp-newer'),
    postcss       = require('gulp-postcss'),
    sass          = require('gulp-sass'),
    cleanCSS      = require('gulp-clean-css'),
    strip         = require('gulp-strip-comments'),
    uglify        = require('gulp-uglify'),
    styleguide    = require('postcss-style-guide'),
    runsequence   = require('run-sequence');

var config = require('./package.json').config;

// Plugin options
var sassOptions = {
  errLogToConsole: true,
  outputStyle: 'expanded'
};

var autoprefixerOptions = {
  browsers: ['last 2 versions']
};

var imgOptions = {
  optimizationLevel: 5,
  progressive: true,
  interlaced: true
}

var prettifyOptions = {
 "indent_size": 2,
 "indent_level": 0,
 "eol": "\n",
 "space_after_anon_function": true,
 "keep_function_indentation": true,
 "preserve_newlines": true,
 "indent_with_tabs": false,
 "wrap_attributes_indent_size": 2,
 "end_with_newline": true,
 "brace_style": "end-expand"
}

// Scss task
gulp.task('styles', function () {
  var task =  gulp.src(config.files.scss.src + '**/*.scss')
                  .pipe(sass(sassOptions).on('error', sass.logError))
                  .pipe(autoprefixer(autoprefixerOptions))

  if (production) {
    task = task.pipe(cleanCSS());
  }

  return task.pipe(gulp.dest(config.files.scss.dist));
});

// JS task
//  Behaviors
gulp.task('scripts', function () {
  var task = gulp.src(config.files.js.srcBehaviors + '*.js')
                .pipe(strip())
                .pipe(insert.prepend('(function ($, window) {\n  \'use strict\';\n'))
                .pipe(insert.append('})(jQuery, window);\n'))
                .pipe(prettify(prettifyOptions))
                .pipe(eslint())
                .pipe(eslint.format())

  if (production) {
      task = task.pipe(uglify());
  }

  return task.pipe(gulp.dest(config.files.js.distBehaviors));
});

//  Vendors
gulp.task('scripts-vendors', function () {
  var task = gulp.src(config.files.js.srcVendor + '*.js')
                .pipe(strip())
                .pipe(insert.prepend('(function ($, window) {\n  \'use strict\';\n'))
                .pipe(insert.append('})(jQuery, window);\n'))

  if (production) {
      task = task.pipe(uglify());
  }

  return task.pipe(gulp.dest(config.files.js.distVendor));
});

// Images task
gulp.task('images', function () {
  var task = gulp.src(config.files.img.src + '**')
        .pipe(newer(config.files.img.dist))
        .pipe(gulp.dest(config.files.img.dist))
        .pipe(imageoptim(imgOptions))
        .pipe(gulp.dest(config.files.img.dist));

  return task;
});

// Fonts task
gulp.task('fonts', function () {
  var task = gulp.src(config.files.fonts.src + '**')
    .pipe(newer(config.files.fonts.dist))
    .pipe(gulp.dest(config.files.fonts.dist));

  return task;
});

// Styleguide task
gulp.task('styleguide', function () {
  var processedCSS = fs.readFileSync(config.files.scss.dist + 'base.css', 'utf-8');

  var task = gulp.src(config.files.scss.dist + '*.css')
      .pipe(postcss([
          styleguide({
              name: "Sassy styleguide",
              processedCSS: processedCSS
          })
      ]))
      .pipe(gulp.dest('docs/'));

  return task;
});

// Watch task
gulp.task('watch',['clean', 'styles', 'fonts', 'images', 'scripts', 'scripts-vendors'], function () {
  // Watch scss
  gulp.watch([config.files.scss.src + '**/**/*.scss'], ['styles']);

  // Watch behaviors
  gulp.watch([config.files.js.src + '**/**/*.js'], ['scripts']);

  // Watch image files
  // gulp.watch([config.files.img.src + '**/**/**'], ['images']);
});

// Clean task => remove the destination folders
gulp.task('clean', function () {
    del([config.files.scss.dist, config.files.js.distBehaviors, config.files.js.distVendor, config.files.img.dist].map(function () {
      return this + '/**';
    }));
});

// Clean production => remove source files
gulp.task('cleanproduction', function () {
    del([config.files.scss.src, config.files.js.distBehaviors, config.files.js.distVendor, config.files.img.src].map(function () {
      return this + '/**';
    }));
});

// Development
gulp.task('development', function (callback) {
  return runsequence('styles', 'scripts', 'scripts-vendors', 'fonts', 'images', callback);
});

// Production
gulp.task('production', ['clean'], function () {
    production = true;
    return runsequence('development', ['cleanproduction']);
});

// Default
gulp.task('default', function () {
  return gulp.start('watch');
});
